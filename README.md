## Atelier n°1 
- [x]  Création du compte GitLab et découverte de l’interface 
- [x] Se créer un compte gratuit sur https://www.gitlab.com/ et mémoriser son identifiant et mot de passe.
- [x] Parcourir l’interface et en particulier les paramètres généraux.

## Atelier n°2 
- [x] Créer un nouveau dossier et y initialiser un dépôt public Git.
- [x] Y créer un fichier README.md et y ajouter du texte.
- [x] Créer un nouveau dépôt sur GitLab et y envoyer ce qui a déjà été créé.
- [x] Créer une branche en local et y faire 2-3 commits (deuxième modif).
- [x] Créer une branche sur GitLab et y faire 2-3 commits en passant par GitLab (deuxième modif).
- [x] Faire un merge de la branche créée sur GitLab sur master dans le terminal.
- [x] Envoyer les modifications sur le dépôt distant.
- [x] Faire un merge de la branche locale dans master dans le terminal.
- [x] Envoyer les modifications sur le dépôt distant.
- [x] Créer une branche sur GitLab et y faire 2-3 commits en passant par GitLab.
- [x] Faire un merge de la branche créée sur GitLab sur master dans le terminal.
- [x] Envoyer les modifications sur le dépôt distant.
- [x] Faire un merge de la branche locale dans master dans le terminal.
- [x] Envoyer les modifications sur le dépôt distant.

```
$ git logall
*   0b80787 - (2 minutes ago) Last commit - Guillaume Martinez (HEAD -> master, origin/master)
|\
| * b636257 - (4 minutes ago) Another commit - Guillaume Martinez (origin/a1.1, a1.1)
* | c75b771 - (7 minutes ago) Merged with branch created in gitlab - Guillaume Martinez
* | 55eb4a0 - (10 minutes ago) Update README.md - Martinez Guillaume (origin/a1.2, a1.2)
* | 869e116 - (11 minutes ago) Update README.md - Martinez Guillaume
|/
* 9b503b0 - (13 minutes ago) second commit a1.1 - Guillaume Martinez
* 216abd2 - (13 minutes ago) first commit a1.1 - Guillaume Martinez
* 8421fe6 - (17 minutes ago) Update README.md - Martinez Guillaume
* 665b078 - (18 minutes ago) Update README.md - Martinez Guillaume
* 524046e - (19 minutes ago) Update README.md - Martinez Guillaume
* d50f2ac - (22 minutes ago) add README - Guillaume Martinez
```
